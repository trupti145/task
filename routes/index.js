var express = require('express');
var router = express.Router();
var models = require('../models')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST server information creation. */
router.post('/serverInformation/create', function(req, res, next) {
let dataObj = {
  version:req.body.version,
  os:req.body.os,
  proces_id:req.body.proces_id,
  up_time:req.body.up_time,
  connection:req.body.connection,
  blocked:req.body.blocked
};
models.serverInformation.create(dataObj).then(data =>{
  console.log("user status created : ");
                    res.status(200).json({
                        status: "success",
                        message: "Server Information created",
                        data: data
                    })
}).catch(error =>{
  res.send({
    error:error
  })
})
});

router.post('/statsInformation/create', function(req, res, next) {
  let dataObj = {
    serverInformationId:req.body.serverInformationId,
    connect_received:req.body.connect_received,
    cmd_prrocessed:req.body.cmd_prrocessed,
    ops_perr_sec:req.body.ops_perr_sec,
    rejected_connect:req.body.rejected_connect,
    expired_keys:req.body.expired_keys,
    evicted_keys:req.body.evicted_keys,
    historymisses: req.body.historymisses
  };
  models.statsInformation.create(dataObj).then(data =>{
    console.log("user status created : ");
                      res.status(200).json({
                          status: "success",
                          message: "Stats Information created",
                          data: data
                      })
  }).catch(error =>{
    res.send({
      error:error
    })
  })
  });

  router.post('/redisDB/create', function(req, res, next) {
    let dataObj = {
      serverInformationId:req.body.serverInformationId,
      db:req.body.db,
      keys:req.body.keys,
      expires:req.body.expires,
      avg_ttl:req.body.avg_ttl,
      serverInformationId:req.body.serverInformationId
    };
    models.redisDB.create(dataObj).then(data =>{
      console.log("user status created : ");
                        res.status(200).json({
                            status: "success",
                            message: "Redis DB created",
                            data: data
                        })
    }).catch(error =>{
      res.send({
        error:error
      })
    })
    });


module.exports = router;
