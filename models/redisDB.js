const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:');

const User = sequelize.define('redisDB', {
  // Model attributes are defined here
  id: {
    type:DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4
},
  serverInformationId:{type:DataTypes.STRING},
  db:{type:DataTypes.STRING},
  keys:{type:DataTypes.INTEGER},
  expires:{type:DataTypes.STRING},
  avg_ttl:{type:DataTypes.INTEGER},
  serverInformationId:{type:DataTypes.INTEGER}
});

// `sequelize.define` also returns the model
console.log(User === sequelize.models.redisDB); 