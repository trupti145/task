const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:');

const User = sequelize.define('serverInformation', {
  // Model attributes are defined here
  id: {
    type:DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4
},
  version:{type: DataTypes.STRING},
  os:{type: DataTypes.STRING},
  proces_id:{type: DataTypes.INTEGER},
  up_time:{type: DataTypes.STRING},
  connection:{type: DataTypes.INTEGER},
  blocked:{type: DataTypes.INTEGER}
});

// `sequelize.define` also returns the model
console.log(User === sequelize.models.serverInformation); 