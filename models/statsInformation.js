const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:');

const User = sequelize.define('statsInformation', {
  // Model attributes are defined here
  id: {
    type:DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4
  },
  serverInformationId:{type:DataTypes.STRING},
  connect_received:{
    type: DataTypes.INTEGER
  },
  cmd_prrocessed:{
    type: DataTypes.INTEGER
  },
  ops_perr_sec:{
    type: DataTypes.INTEGER
  },
  rejected_connect:{
    type: DataTypes.INTEGER
  },
  expired_keys:{
    type: DataTypes.INTEGER
  },
  evicted_keys:{
    type: DataTypes.INTEGER
  },
  historymisses:{
    type: DataTypes.INTEGER
  }
});

// `sequelize.define` also returns the model
console.log(User === sequelize.models.statsInformation); 